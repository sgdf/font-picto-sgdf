# Pictos Scouts et Guides de France

# Présentation

Ce projet permet de créer un package npm avec les pictogrames de l'identité numérique des [Scouts et Guides de France](https://www.sgdf.fr).

Le package final contient:
- une font au format True Type, woff ou woff2
- les pictos individuels au format svg

![Pictos disponibles](https://gitlab.com/sgdf/font-picto-sgdf/-/raw/master/images/pictos.png?raw=true)

# Installation

Le package est hébergé sur le serveur de gitlab, il est donc nécessaire d'indiquer à npm où il se trouve.

```
npm install --save --registry=https://gitlab.com/api/v4/packages/npm @sgdf/font-picto
```

Dans un projet complet, editer le fichier .npmrc et rajouter :
```
@sgdf:registry=https://gitlab.com/api/v4/packages/npm
```

# Utilisation

Les pictogrammes sont tous en noir, il est donc nécessaire de leur appliquer une classe supplémentaire pour ajouter la couleur désirée.

Un fichier css "sgdf-pictos-commun.css" contient quelques classes qui permettent d'appliquer les couleurs officielles :
- sgdf-picto-couleur-commun
- sgdf-picto-couleur-compagnons
- sgdf-picto-couleur-farfadets
- sgdf-picto-couleur-pionniers-caravelles
- sgdf-picto-couleur-scouts-guides
- sgdf-picto-couleur-louveteaux-jeanettes
- sgdf-picto-couleur-audace

En entête pour charger les couleurs et la font:
```
<link rel="stylesheet" type="text/css" href="sgdf-pictos-commun.min.css" />
<link rel="stylesheet" type="text/css" href="sgdf-pictos.min.css" /
```

Dans un scss, il suffit de faire :
```
@import '~@sgdf/font-picto/css/sgdf-pictos-commun.min.css';
@import '~@sgdf/font-picto/font/sgdf-pictos.min.css';

```

Pour utiliser la croix scoute dans une page:
```
<i class="sgdf-picto sgdf-picto-signesCommuns-CroixScoute"></i>
```

Pour utiliser la croix scoute en vert "compagnons" dans une page:
```
<i class="sgdf-picto sgdf-couleur-compas sgdf-picto-signesCommuns-CroixScoute"></i>
```

# Pictos disponibles

Voir [ce lien] (https://sgdf.gitlab.io/font-picto-sgdf/) pour les pictos disponibles

# Historique des versions
Voir [CHANGELOG](https://gitlab.com/sgdf/font-picto-sgdf/-/raw/master/CHANGELOG) pour les détails

# License
MIT, voir [LICENSE](https://gitlab.com/sgdf/font-picto-sgdf/-/raw/master/LICENSE) pour les détails
